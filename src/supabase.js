import { createClient } from '@supabase/supabase-js'

const base = createClient( process.env.supabaseURL, process.env.apikey)

export default base

export const mixin = {
    created(){
        if(base.auth.session() != null){
            //on page reload if there is a token
            //this is the way to know if the user is authenticated
            this.$store.dispatch('load')
        }
    }
}