import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/home/index'
import Stocks from '@/components/market/stocklist'
import Portfolio from '@/components/market/portfolio'
import Auth from '@/components/auth'
import Options from '@/components/options'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/options',
      name: 'Options',
      component: Options
    },
    {
      path: '/stocks',
      name: 'Stocks',
      component: Stocks
    },
    {
      path: '/portfolio',
      name: 'Portfolio',
      component: Portfolio
    },
    {
      path: '/auth',
      name: 'Auth',
      component: Auth,
      children: [
        {
          path: 'signin',
          name: 'SignIn',
          component: Auth
        },
        {
          path: 'recovery',
          name: 'Recovery',
          component: Auth
        },
        {
          path: 'invite',
          name: 'Invite',
          component: Auth
        }
      ]
    },
    {
      path: '/access_token=:token&expires_in=:expires&refresh_token=:refresh_token&token_type=:token_type&type=:access_type',
      name: 'Auth',
      component: Auth
    }
  ]
})
