import Vue from "vue"
import Vuex from "vuex"
import Market from "./modules/market"
import Logger from "./modules/logger"
import Users from "./modules/users"

Vue.use( Vuex )

export const store = new Vuex.Store({
    modules: {
        Market,
        Logger,
        Users
    },
    //in case a 'pasta' effect is noticed between any of these actions and the actions of a module
    //these actions are general actions. think of them as 'root' actions
    //they might be called in any of the modules above
    actions: {
        load({dispatch, commit}){
            commit('signedIn', true)
            dispatch('getUser')
            dispatch('getStocks')
            dispatch('getPortfolio')
        },
        unload({dispatch, commit}){
            commit('setUserInfo', {})
            commit('clearMarketData')
            commit('signedIn', false)
        }
    }
})