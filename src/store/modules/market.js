import supabase from '../../supabase'
import _ from "lodash"

const state = {
    funds: 0,
    portfolio: {},
    stocks: {}
}

const getters = {
    getStocks(state){
        return state.stocks
    },
    getStock(state){
        return (id) => {
            return state.stocks[id]
        }
    },
    getPortfolio(state){
        return state.portfolio
    },
    getAsset(state){
        return (id) => {
            return state.portfolio[id]
        }
    },
    getFunds(state){
        return state.funds
    }
}

function setAsset(state, {id, quantity}){
    if( typeof state.stocks[id] == 'undefined' ) return;

    if(typeof state.portfolio[id] == 'undefined'){
        state.portfolio[id] = {
            id,
            name: state.stocks[id].name,
            value: state.stocks[id].value,
            quantity: 0
        }
    }

    state.portfolio = {
        ...state.portfolio,
        [id]: {
            ...state.portfolio[id],
            value: Number(state.stocks[id].value),
            quantity: Number(state.portfolio[id].quantity) + Number(quantity)
        }
    }
}

const mutations = {
    clearMarketData(state){
        state.funds =  0
        state.portfolio = {}
        state.stocks = {}
    },
    setStock(state, { id, name, quantity, value }){
        state.stocks[id] = { ...state.stocks[id], id, name, quantity, value }
        if(typeof state.portfolio[id] != 'undefined')
            state.portfolio[id] = { ...state.portfolio[id], value }
    },
    setFunds(state, {value, total}){
        if(typeof total != 'undefined'){
            state.funds = total
            return
        }
        if( (state.funds + value) > 0 )
            state.funds += value
        else
            state.funds = 0
    },
    setAsset,
    buyAsset(state, {id, quantity}){
        if( typeof state.stocks[id] == 'undefined' ) return;

        if( quantity < 0 || state.stocks[id].quantity < quantity || (state.stocks[id].value * quantity) > state.funds ) return;

        setAsset(state, {id, quantity})

        state.funds -= Number(state.stocks[id].value) * Number(quantity)
        state.stocks = {
            ...state.stocks,
            [id]: {
                ...state.stocks[id],
                quantity: Number(state.stocks[id].quantity) - Number(quantity)
            }
        }
    },
    sellAsset(state, {id, quantity}){
        if( typeof state.stocks[id] == 'undefined' || typeof state.portfolio[id] == 'undefined' || quantity < 0 || state.portfolio[id].quantity < quantity ) return;

        let diff = Number(state.portfolio[id].quantity) - Number(quantity)

        if(diff == 0){
            let newportfolio = {
                ...state.portfolio
            }
            delete newportfolio[id]
            state.portfolio = newportfolio
        }else{
            state.portfolio = {
                ...state.portfolio,
                [id]: {
                    ...state.portfolio[id],
                    value: Number(state.stocks[id].value),
                    quantity: diff
                }
            }            
        }
        
        state.funds += Number(state.stocks[id].value) * Number(quantity)
        state.stocks = {
            ...state.stocks,
            [id]: {
                ...state.stocks[id],
                quantity: Number(state.stocks[id].quantity) + Number(quantity)
            }
        }
    },
    endDay(state){
        let stocks = {
            ...state.stocks
        }
        let portfolio = {
            ...state.portfolio
        }
        for(let code in stocks){
            let modifier = Number((Math.random() * (2.000 - -2.000) + -2.000).toFixed(2)) / 100
            stocks[code].value = Number((stocks[code].value + (stocks[code].value * modifier)).toFixed(2))
            if( typeof portfolio[code] != 'undefined' ) portfolio[code].value = stocks[code].value
        }
        state.stocks = stocks
        state.portfolio = portfolio
    }
}

const actions = {
    async getStocks({commit}){

        const { data, error } = await supabase.from('stocks').select('*')

        if(error == null){

            if(data instanceof Array)
                for(const row of data){
                    commit('setStock', row)
                }
            else commit('setStock', data)
    
        }else{

            commit('setError', { category: 'market', title: 'Stocks server error', message: 'The stocks could not be fetched from server'})

        }

    },
    async getPortfolio({commit}){

        const { data, error } = await supabase.from('portfolio').select('*')

        if(error == null){

            if(data instanceof Array)
                for(const row of data){
                    commit('setAsset', {id: row.stock, quantity: row.quantity})
                }
            else commit('setAsset', {id: data.stock, quantity: data.quantity})
    
        }else{

            commit('setError', { category: 'market', title: 'Portfolio server error', message: 'The portfolio could not be fetched from server'})

        }

    },
    setStock({commit}, { id, name, quantity, value }){
        commit('setStock', { id, name, quantity, value })
    },
    async buyAsset({commit}, {id, quantity}){
        if( typeof state.stocks[id] == 'undefined' ){
            commit('setError', { category: 'buy', title: 'No such stock', message: 'The stock is not registered in the stock list'})
            return
        }

        if( quantity < 0 || state.stocks[id].quantity < quantity || (state.stocks[id].value * quantity) > state.funds ){
            commit('setError', { category: 'buy', title: 'Invalid quantity', message: 'The amount of money is not enough to buy these assets'})
            return
        }

        const { data, error } = await supabase.rpc('buy_asset', {
            given_stock: id, 
            add_quantity: quantity
        })
        
        if( error != null){
            commit('setError', { category: 'buy', title: 'DB error', message: error})
            return
        }
        commit('buyAsset', {id, quantity})
    },
    async sellAsset({commit}, {id, quantity}){
        if( typeof state.stocks[id] == 'undefined'){
            commit('setError', { category: 'sell', title: 'No such stock', message: 'The stock is not registered in the stock list'})
        }
        if(typeof state.portfolio[id] == 'undefined'){
            commit('setError', { category: 'sell', title: 'No such asset in portfolio', message: 'The asset is not in portfolio, therefore can not be sold.'})
        }
        if(quantity < 0 || state.portfolio[id].quantity < quantity ){
            commit('setError', { category: 'sell', title: 'Invalid quantity', message: 'The number of assets to sell are greater than the number of assets owned'})
        }
        
        const { data, error } = await supabase.rpc('sell_asset', {
            given_stock: id, 
            sub_quantity: quantity
        })
        
        if( error != null){
            commit('setError', { category: 'buy', title: 'DB error', message: error})
            return
        }
        commit('sellAsset', {id, quantity})
    },
    endDay({commit}){
        commit('endDay')
    }
}

export default {
    state, getters, mutations, actions
}