import supabase from '../../supabase'

const state = {
    user: {},
    signedIn: false
}

const getters = {
    getUserInfo(state){
        return state.user
    },
    isSignedIn(state){
        return state.signedIn
    }
}

const mutations = {
    setUserInfo(state, info){
        state.user = info
    },
    signedIn(state, status){
        state.signedIn = status
    }
}

const actions = {
    async signUp({dispatch, commit}, {username, email, password}){

        try{
            const { user, error } = await supabase.auth.signUp({
                email,
                password,
                data: { username }
            })

            if(error == null){
                dispatch('load')
            }
        }catch(exception){
            console.log(exception)
        }

    },
    async signIn({dispatch, commit}, {email, password}){

        try{
            const { user, error } = await supabase.auth.signIn({
                email,
                password
            })

            if(error == null){
                dispatch('load')
            }
        }catch(exception){
            console.log(exception)
        }

    },
    async signOut({commit}){

        try{
            const { error } = await supabase.auth.signOut()
            if(error == null){
                dispatch('unload')
            }
        }catch(exception){
            console.log(exception)
        }

    },
    async updateUser({commit}, {token, password}){

        try{
            const { data, error } = await supabase.auth.api.updateUser(token, { password })
        }catch(exception){
            console.log(exception)
        }

    },
    async getUser({commit}){

        try{
            const { data, error} = await supabase.rpc('get_user', {id: supabase.auth.user().id})
            if(error == null){
                commit('setUserInfo', {
                    id: supabase.auth.user().id,
                    email: data.data.email,
                    data: data.data
                })
                commit('setFunds', {value:data.funds})
            }
        }catch(exception){
            console.log(exception);
        }
        
    },
    signedIn({commit}, {status}){
        commit('signedIn', status)
    }
}

export default {
    state, getters, mutations, actions
}